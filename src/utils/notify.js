import { toast } from "react-toastify";

/**
 * @param {string} message
 * @param {import("react-toastify").TypeOptions} type
 */
const notify = (message, type) => {
	switch (type) {
		case "error":
			return toast.error(message);
		case "info":
			return toast.info(message);
		case "success":
			return toast.success(message);
		case "warning":
			return toast.warning(message);
		case "default":
			return toast(message);
		default:
			return toast(message);
	}
};

export default notify;
