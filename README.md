# MI Boilerplate NextJS

A ready to use Next JS boilerplate made specifically for MadeIndonesia React projects.

## Quick Overview

Technology used in this boilerplate is:

- [Javascript](https://www.javascript.com/)
- [Next JS](https://nextjs.org/docs/getting-started)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Scss](https://sass-lang.com/)
- [Axios](https://axios-http.com/docs/intro)

We use [Atomic Design](https://atomicdesign.bradfrost.com/chapter-2/) for component structure, API layering, and feature based folder.

We recommend you to follow [SOLID principles](https://dickymadeindonesia.gitbook.io/mi-react-guide/solid-in-react) with this boilerplate.

This boilerplate also comes with [eslint](https://eslint.org/), [prettier](https://prettier.io/), [husky](https://typicode.github.io/husky/#/) and [lint-staged](https://www.npmjs.com/package/lint-staged/v/8.1.3) so you don't have to worry about code styling as long as you follow the rules. But feel free to modify it to your liking.

## Getting Started

Use [mi-create-nextjs](https://gitlab.com/candidate_public/mi-boilerplate-nextjs) for easier setup with zero configuration.

```bash
# Install nextjs boilerplate
npx mi-create-nextjs my-app
cd my-app

# Install the packages
npm install

# Run project locally
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) or wherever NextJS start the server with your browser to see the result.

## Learn More

To learn more about this boilerplate, check [MI React Guide. Learn the basics of this boilerplate structure](https://dickymadeindonesia.gitbook.io/mi-react-guide/).
