import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import store from "src/store";
import "../styles/globals.css";

const App = ({ Component, pageProps }) => {
	return (
		<>
			<ToastContainer autoClose={3000} position="bottom-left" limit={3} />
			<Provider store={store}>
				<Component {...pageProps} />
			</Provider>
		</>
	);
};

export default App;
